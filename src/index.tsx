import {createRoot} from "react-dom/client";
import {FC, StrictMode} from "react";
import React from "react";
import App from "./App";

const container = document.getElementById("_root");
const root = createRoot(container);


root.render(
    <StrictMode>
        <App title="Хола хола"/>
    </StrictMode>
)