import React, {FC} from 'react';

type App = {
    title: string
}
const App: FC<App> = ({title}) => {
    return (
        <div>
            {title && <h1>{title}</h1>}
        </div>
    );
};

export default App;