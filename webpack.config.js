const path = require('path');
const fork_ts_check_wp_plugin = require('fork-ts-checker-webpack-plugin');
const html_webpack_plugin = require('html-webpack-plugin');

module.exports = {
    mode: "development",
    target: "web",
    entry: path.resolve(__dirname, 'src/', 'index.tsx'),
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "[name].js"
    },
    module: {
        rules: [
            {
                test: /\.(js|mjs|jsx|ts|tsx)$/,
                use: 'babel-loader',
                exclude: /node_modules/,
            },
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    plugins: [
        new fork_ts_check_wp_plugin({
            typescript: {
                diagnosticOptions: {
                    semantic: true,
                    syntactic: true
                }
            }
        }),
        new html_webpack_plugin({
            template: path.resolve(__dirname, 'public', 'index.html'),
            inject: true
        }),
    ],
    devServer: {
        hot: true,
        open: true,
        static: {
            directory: path.resolve(__dirname, 'public')
        },
        compress: true,
        port: 9000,
        client: {
            overlay: true,
            progress: true,
        }
    },
    devtool: 'inline-source-map',
}